module MinHS.Evaluator where
import qualified MinHS.Env as E
import qualified Data.Maybe as MB
import MinHS.Syntax
import MinHS.Pretty
import qualified Text.PrettyPrint.ANSI.Leijen as PP

type VEnv = E.Env Value

data Value = I Integer
           | B Bool
           | Nil
           | Cons Integer Value
           | Clos VEnv Bind
           | Par Op [Value]
           | ParList Id [Value]
           deriving (Show)

instance PP.Pretty Value where
  pretty (I i) = numeric $ i
  pretty (B b) = datacon $ show b
  pretty (Nil) = datacon "Nil"
  pretty (Cons x v) = PP.parens (datacon "Cons" PP.<+> numeric x PP.<+> PP.pretty v)
  pretty _ = undefined -- should not ever be used

evaluate :: Program -> Value
evaluate bs = evalE E.empty (Let bs (Var "main"))

evalE :: VEnv -> Exp -> Value

evalE g (Let [Bind "main" t d e] b) = evalE g e

-- Matches lists of args d for Task 5: let bindings declare functions
evalE g (Let b ex) = case b of
    (Bind v t d e):xs -> case xs of
        [] -> case d of
            [] -> evalE (E.add g (v, evalE g e ) ) ex
            _ -> evalE (E.add g (v, Clos g (Bind v t d e))) ex
        _  -> case d of
            [] -> evalE (E.add g (v, evalE g e ) ) (Let xs ex)
            _  -> evalE (E.add g (v, Clos g (Bind v t d e))) (Let xs ex)

-- Lazy evalulates list of bindings for Task 6: Mutually recursive bindings
evalE g (Letrec b ex) = case b of
    [] -> evalE g ex
    (Bind v t d e):xs  -> case xs of
        [] -> evalE (E.add g (v, evalE g e)) ex
        _  -> evalE g' ex
            where g' = E.addAll g (map tupl b)
                  tupl (Bind v t d e) = (v, evalE g' e) 

-- Primitive operations including Task 2 Partial Primops.
evalE g (Prim o) = Par o []
evalE g (Con "Cons") = ParList "Cons" []
evalE g (Con "Nil") = Nil
evalE g (Con "True")  = B True
evalE g (Con "False")  = B False
evalE g (Num i) = I i


evalE g (App e1 e2) =
    case (evalE g e1, evalE g e2) of
        -- Task 3: n-ary functions by bindnig each argument in closure.
        (Clos env1 (Bind f t i e), v2) -> case i of
            [] -> evalE g (App e e2)
            x:xs -> case xs of 
                [] -> evalE (E.addAll env1 [(f, (evalE g e1)), (x, v2)]) e
                _  -> Clos (E.addAll env1 [(f, (evalE g e1)), (x, v2)]) (Bind f t xs e)

        -- Matches the number of elements in [Value] to tell if function is saturated yet.
        (Par Neg [], I n2) -> I(-n2)

        (Par Head [], x ) -> case x of
            Nil        -> error "head of empty list"
            (Cons i x) -> I i
            Clos e (Bind id t d ex) -> evalE e (App (Prim Head) ex)

        (Par Tail [], x ) -> case x of
            Nil -> error "tail of empty list"
            (Cons i x) -> x
            Clos e (Bind id t d ex) -> case ex of
                (App (App (Con "Cons") i) x) -> evalE g x

        (Par Null [], x ) -> case x of
            Nil -> B True
            _   -> B False

        (Par o [], I n2) -> Par o [(I n2)]
        (Par Add [(I n1)], I n2) -> I( n1 + n2 )
        (Par Sub [(I n1)], I n2) -> I( n1 - n2 )
        (Par Mul [(I n1)], I n2) -> I( n1 * n2 )
        (Par Quot [(I n1)], I n2) -> I( quot n1 n2 )
        (Par Rem [(I n1)], I n2) -> I( mod n1 n2 )
        (Par Gt [(I n1)], I n2) -> B( n1 > n2 )
        (Par Lt [(I n1)], I n2) -> B( n1 < n2 )
        (Par Le [(I n1)], I n2) -> B( n1 <= n2 )
        (Par Ge [(I n1)], I n2) -> B( n1 >= n2 )
        (Par Eq [(I n1)], I n2) -> B( n1 == n2 )
        (Par Ne [(I n1)], I n2) -> B( n1 /= n2 )

        (ParList "Cons" [], v2) -> ParList "Cons" [v2]
        (ParList "Cons" [(I i)], v2) -> Cons i v2

evalE g (Var v) =
    case E.lookup g v of
        a -> case MB.isNothing a of
            False -> MB.fromJust a
            True -> error v

evalE g (Letfun b) = Clos g b

evalE g (If c t e) =
    case evalE g c of
        B True -> evalE g t
        B False -> evalE g e

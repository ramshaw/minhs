module Paths_minhs (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,1,0,0], versionTags = []}
bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/Users/aaron/Library/Haskell/ghc-7.6.3/lib/minhs-0.1.0.0/bin"
libdir     = "/Users/aaron/Library/Haskell/ghc-7.6.3/lib/minhs-0.1.0.0/lib"
datadir    = "/Users/aaron/Library/Haskell/ghc-7.6.3/lib/minhs-0.1.0.0/share"
libexecdir = "/Users/aaron/Library/Haskell/ghc-7.6.3/lib/minhs-0.1.0.0/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catchIO (getEnv "minhs_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "minhs_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "minhs_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "minhs_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)

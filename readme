Aaron's COMP3161 Assignment 1 14s2

MinHS interpreter
Due Fri 26 Sep 2014

From the spec:
In this assignment you will implement an interpreter for MinHs, a small functional language similar to ML and Haskell. It is fully typed, with types specified by the programmer. The assignment consists of a base compulsory component, worth 70%, and five additional components, each worh 10%, which collectively are worth 50%, meaning that only three of them have to be completed to earn full marks. You can earn bonus marks by completing more tasks.


Task 1:
Implement an interpreter for the MinHs language presented in the lectures, using an environment semantics, including support for recursion and closures.

The front end of the interpreter (lexer, parser, type checker) is provided for you, along with the type of the evaluate function (found in the file Evaluator.hs). The function evaluate returns an object of type Value. You may modify the constructors for Value if you wish, but not the type for evaluate. The return value of evaluate is used to check the correctness of your assignment.

You must provide an implementation of evaluate, in Evaluator.hs. It is this file you will submit for Task 1. No other files can be modified.

You can assume the typechecker has done its job and will only give you correct programs to evaluate. The type checker will, in general, rule out incorrect programs, so the interpreter does not have to consider them.


Running MinHS:
```
#!sh

cabal update
cabal install --only-dependencies
cabal configure
cabal build
./dist/build/minhs-1/minhs-1 --help
./dist/build/minhs-1/minhs-1 foo.mhs
./dist/build/minhs-1/minhs-1 --dump parser-raw
./run-tests.sh
./run-tests.sh | grep "Running test"
```